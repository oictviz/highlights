# Security Council Highlights 2015 #


## Introduction ##

* This is a web app using [Qlik Sense Mashup API](https://help.qlik.com/sense/2.0/en-us/developer/#../Subsystems/Mashups/Content/mashups-introduction.htm%3FTocPath%3DBuilding%2520mashups%7C_____0) and Bootstrap JS framework. The data being used in this visualization is the 2015 study for the UN Security Council, obtained from UN Department of Political Affairs - Security Council Practices and Charter Research Branch, and the Qlik file is hosted on the production internal server.
* Version : 11.0
* [Live Demo](https://unite.un.org/sites/unite.un.org/files/app-schighlights/index.html)

## Setting up ##
### Requirement ####
* Local web server installed on your local machine. For development in Windows OS, [Baby Web Server](http://www.pablosoftwaresolutions.com/html/baby_web_server.html) is a recommended option
* Modern browser that supports HTML5 (Chrome, Firefox, etc).

### Updating Guidelines ####
**Updating Qlik Objects**

* When updating the excel files, make sure all the fields are the same with the previous version .xls file unless. Failure to follow this procedure will resulted in creating new chart thus not replacing the chart in the mashup.
* Make sure all the field name on the excel files are unique so it doesn't interact with each other when filter is applied on the charts.
* If possible, use same color for all the charts in the web page to prevent overcluttering.
* The word cloud on web app is a static image, screenshotted from chart generated from [Qlik Sense Extension](https://community.qlik.com/docs/DOC-7794). Make sure this extension is installed in your Qlik local server to create the image.

**Updating Code**

#### Setting Up #####
To deploy this web app on your machine, if you are familiar using git, clone the repository on your local web server root directory : 
```
git clone https://<YOUR BITBUCKET USERNAME>@bitbucket.org/oictviz/highlights.git

```
or, download this repository and put this code on local web server root directory.

Navigate to this URL :
```
<server>:<port>/Highlights/index.html
```
 
* When updating the text file, replace the text in .html files into the newest text provided by the client according to the language provided (index.html for English, french.html for French).
* Make sure to change web page links, introductory text on Internet Explorer handler, and links to pdf version of the report as well as the annex.
* If the chart's new version has new value, make sure to add its legends as with its corresponding colors in the .html file.
* Make sure to replace word cloud images with new version generated from the newest data.

**Updating PDF**

The pdf version of this web application is generated using Internet Explorer 11 'Save as PDF' capabaility. This step has to be done in IE 11 browser because it supports rendering images from Qlik Objects. After you finished updating the Qlik Objects and Web App code, navigate to index.html, Go to Settings --> Save as PDF. You might need to adjust the pagination, pdf title, and other elements of the pdf according to the requirement.


### Contact ###
* Creator : Daniela Marin Puentes (marinpuentes@un.org)
* Amendments : Kania Azrina (azrina@un.org)