/**
 * @author Daniela Marin Puentes
 * Date: July 2015
 * Description: Load qlik objects and handle interactivity
 * Amendments: Kania Azrina
 **/

/* Variable declaration */
var app; //web app
var qlikApp; //qlik objects
var width = screen.width; //screen width
var charts_Dict = {};
var charts_EN = {
  /* Mapping between Qlik object ID and chart name */
  /* Meetings */
  'Meetings-Overall': "RGUCg", //chart1
  'Meetings-Monthly': 'jNmwE', //chart2
  'Meetings-highlevel': 'mteQjH', //chart3
  'Meetings-Arr': 'Afkra', //chart5
  'Meetings-VTC': 'zkjrry', //chart4
  'Meetings-whole': 'RTYRXk', //chart6 old
  /* Missions */
  'Missions-Overall': 'dJGHwJ', //chart6
  /* Agenda */
  'Agenda-Overall': 'gxkHJP', //chart7
  'Agenda-Number': 'TXkMjRp', //chart8
  'Agenda-Regions': 'FguBL', //chart9
  /* Decisions */
  'Decisions-type': 'MYk', //chart10
  'Decisions-Regions': 'tdhC', //chart11
  'Decisions-Voting': 'FrXJkFh', //chart12
  'Decisions-CCAC': 'teygHUQ', //chart13
  'Decisions-ChVII': 'dySZQNn', //chart14
  /* Subsidiary */
  'Subsidiary-Meetings': 'ptT', //chart15
  'Subsidiary-Entities': 'hEhWsAb', //chart16
  'Subsidiary-Delistings': 'wPmhdU', //chart17
  'Subsidiary-FocalPoint': 'MYLqFP', //chart18
  'Subsidiary-Ombudsperson': 'Mwxrzfa', //chart19
  'Subsidiary-Extensions': 'WQxHJfX' //chart20
};

var charts_FR = {
  /* Mapping between Qlik object ID and chart name */
  /* Meetings */
  'Meetings-Overall': "kpkkpn", //chart1
  'Meetings-Monthly': 'enhCRXP', //chart2
  'Meetings-highlevel': 'PpktwN', //chart3
  'Meetings-Arr': 'fMXRrj', //chart5
  'Meetings-VTC': 'fPPPtm', //chart4
  'Meetings-whole': 'wEAGP', //chart6 old
  /* Missions */
  'Missions-Overall': 'VCSrBsk', //chart6
  /* Agenda */
  'Agenda-Overall': 'PpXCF', //chart7
  'Agenda-Number': 'JwmdwjY', //chart8
  'Agenda-Regions': 'csxLB', //chart9
  /* Decisions */
  'Decisions-type': 'sEfru', //chart10
  'Decisions-Regions': 'GcpPERf', //chart11
  'Decisions-Voting': 'UyEhnu', //chart12
  'Decisions-CCAC': 'xPPVaJ', //chart13
  'Decisions-ChVII': 'JRsm', //chart14
  /* Subsidiary */
  'Subsidiary-Meetings': 'pmMCTKP', //chart15
  'Subsidiary-Entities': 'PbhJsV', //chart16
  'Subsidiary-Delistings': 'syCMfm', //chart17
  'Subsidiary-FocalPoint': 'EmRaUb', //chart18
  'Subsidiary-Ombudsperson': 'DqAzHNY', //chart19
  'Subsidiary-Extensions': 'TnscMy' //chart20
};

var chartMap = {
  /* Meetings */
  'Meetings-Overall' : 1,
  'Meetings-Monthly': 2,
  'Meetings-highlevel': 3,
  'Meetings-VTC': 4,
  'Meetings-Arr': 5,
  /* Missions */
  'Missions-Overall': 6,
  /* Agenda */
  'Agenda-Overall': 7,
  'Agenda-Number': 8,
  'Agenda-Regions': 9,
  /* Decisions */
  'Decisions-type': 10,
  'Decisions-Regions': 11,
  'Decisions-Voting': 12,
  'Decisions-CCAC': 13,
  'Decisions-ChVII': 14,
  /* Subsidiary */
  'Subsidiary-Meetings': 15,
  'Subsidiary-Entities': 16,
  'Subsidiary-Delistings': 17,
  'Subsidiary-FocalPoint': 18,
  'Subsidiary-Ombudsperson': 19,
  'Subsidiary-Extensions': 20
};

var orHeight = [];

function displayImage(chartName){
  var id = chartMap[chartName];
  var img = document.createElement("img");
  img.src = 'images/charts/chart'+id+'.png';
  img.style.width = "100%";
  img.style.height = "auto";
  img.onload = function() {
    chartDiv = document.getElementById(chartName);
    orHeight[chartName] = chartDiv.style.height;
    chartDiv.appendChild(img);
    var divHeight = this.clientHeight;
    chartDiv.style.height = divHeight+"px";
  }
}


function setHeight(chartName){
  chartDiv = document.getElementById(chartName);
  chartDiv.style.height = orHeight[chartName];
}

var lang = document.documentElement.lang;
if (lang =="fr"){
  charts_Dict = charts_FR;
  $('#frenchItem').hide();
  $('#englishItem').show();
} else {
  charts_Dict = charts_EN;
  $('#englishItem').hide();
  $('#frenchItem').show();
}

/* HIDE BUTTONS */
$('#meetingsButton').hide();
$('#agendaButton').hide();
$('#decisionsButton').hide();
$('#subsButton').hide();

var config = {
  host: 'viz.unite.un.org',
  prefix: "/visualization/",
  port: null,
  isSecure: true
};

require.config({
  baseUrl: (config.isSecure ? "https://" : "http://")
  + config.host
  + (config.port ? ":"
  + config.port : "")
  + config.prefix
  + "resources"
});



require(["js/qlik"], function(qlik) {
  qlikApp = qlik;
  qlik.setOnError(function(error) {
    console.log(error.message);
  });

  /* Establish connection with Qlik Server */
  app = qlikApp.openApp("4661e6a3-ed84-44bc-bae5-973bd83a153d", config);
  if (lang =="fr"){
    qlik.setLanguage('fr');
  } else {
    qlik.setLanguage('en');
  }

  /*Set Qlik Language*/

  /* Only getting charts needed on the first load */
  /* Meetings */
  displayObject('Meetings-Overall', true);
  displayObject('Meetings-Monthly', true);
  displayObject('Meetings-highlevel',true);
  displayObject('Meetings-Arr',true);
  displayObject('Meetings-VTC',true);
  /* Missions */
  displayObject('Missions-Overall', true);
  /* Agenda */
  displayObject('Agenda-Overall', false);
  displayObject('Agenda-Number',false);
  displayObject('Agenda-Regions',false);
  /* Decisions */
  displayObject('Decisions-type', true);
  displayObject('Decisions-Regions', false);
  displayObject('Decisions-Voting',true);
  displayObject('Decisions-ChVII',false);
  displayObject('Decisions-CCAC',false);
  /*Subsidiary Bodies*/
  displayObject('Subsidiary-Meetings', false);
  displayObject('Subsidiary-Entities',false);
  displayObject('Subsidiary-Delistings',true);
  displayObject('Subsidiary-FocalPoint',true);
  displayObject('Subsidiary-Ombudsperson',true);
  displayObject('Subsidiary-Extensions',true);



  /* Clear all selection */
  $("#clearSelection").click(function() {
    app.clearAll();
  });

});

function hideElement(elementId) {
    /* Hide element after page loads */
  $('#' + elementId).hide();
}

function removeImg(chartName){
  var chartDiv = document.getElementById(chartName);
  console.log(chartDiv);
  chartDiv.removeChild(chartDiv.childNodes[0])
}

function displayObject(key,mobile) {
  /* Display qlik objects */
  /* If mobile == false, not responsive on small screen */
  if (mobile == false){
    if (width < 480) {
      app.getObject(key, charts_Dict[key], {
        noInteraction: true
      });
    } else {
      app.getObject(key, charts_Dict[key]);
    }
  } else {
    app.getObject(key, charts_Dict[key]);
  }
}

function printObj(obj) {
  var newWindow = window.open(obj);
  newWindow.print();
}
